﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
  public GameObject[] objectsToDisable;

  [SerializeField, HideInInspector]
  private Dropdown combo;
  [SerializeField, HideInInspector]
  private InputField editor;
  [SerializeField, HideInInspector]
  private Slider slider;
  [SerializeField, HideInInspector]
  private GameOfLife gameOfLife;

  private void Awake()
  {
    ComboModified();
    EditorModified();
    SliderModified();
  }

  public void ComboModified()
  {
    bool toEnable = combo.value == 0;
    for (int i = 0; i < objectsToDisable.Length; i++) objectsToDisable[i].SetActive(toEnable);
    gameOfLife.Init((GameOfLife.Mode)combo.value);
  }

  public void EditorModified()
  {
    gameOfLife.SetSize(int.Parse(editor.text));
    gameOfLife.Init(gameOfLife.mode);
  }

  public void SliderModified()
  {
    gameOfLife.updateInterval = 1f/slider.value;
  }
}
