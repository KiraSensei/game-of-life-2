﻿using UnityEngine;
using UnityEngine.UI;

public class GameOfLife : MonoBehaviour
{
  public int size = 100;
  public RenderTexture renderTexture;
  public Material mat;
  public Text textFPS;
  public Texture2D[] patterns;
  public Mode mode;
  public float updateInterval = 1f / 40f;
  public Renderer renderer;

  public enum Mode
  {
    Random,
    Manual,
    Glider,
    SmallSpaceship,
    MiddleSpaceship,
    BigSpaceship,
    Pulsar,
    GosperGun
  };

  private RenderTexture tmpTexture;
  private bool gamePaused = false;
  private float lastUpdateTime = 0f;
  private int nbFPS = 0;
  private float timerFPS = 0;

  // Start is called before the first frame update
  void Start()
  {
    Init(Mode.Random);
  }

  public void Init(Mode newMode)
  {
    if (newMode == Mode.Random) // Random image
    {
      GenerateRandomImage();
    }
    else if (newMode == Mode.Manual)
    {

    }
    else
    {
      renderTexture = new RenderTexture(100, 100, 0)
      {
        wrapMode = TextureWrapMode.Clamp,
        filterMode = FilterMode.Point,
        anisoLevel = 0
      };
      gamePaused = true;
      Texture2D newTexture = patterns[(int)newMode - 2];
      Graphics.Blit(newTexture, renderTexture, mat);
    }
    mat.mainTexture = renderTexture;
    renderer.material.mainTexture = renderTexture;

    mode = newMode;
    tmpTexture = new RenderTexture(renderTexture.width, renderTexture.height, renderTexture.depth, renderTexture.format);
  }

  // Update is called once per frame
  void Update()
  {
    if (gamePaused) return;

    // Image computing
    lastUpdateTime += Time.deltaTime;
    if (lastUpdateTime > updateInterval)
    {
      SwitchTexture();
      lastUpdateTime -= updateInterval;

      // FPS computing
      ++nbFPS;
      if (Time.time - timerFPS > 1f) DisplayFPS();
    }
  }

  private void GenerateRandomImage()
  {
    renderTexture = new RenderTexture(size, size, 0)
    {
      wrapMode = TextureWrapMode.Clamp,
      filterMode = FilterMode.Point,
      anisoLevel = 0
    };
    Texture2D newTexture = new Texture2D(size, size)
    {
      wrapMode = TextureWrapMode.Clamp,
      filterMode = FilterMode.Point
    };

    for (int i = 0; i < size; i++)
    {
      for (int j = 0; j < size; j++)
      {
        int value = Random.Range(0, 2);
        newTexture.SetPixel(i, j, value == 1 ? Color.white : Color.black);
      }
    }
    newTexture.Apply();

    Graphics.Blit(newTexture, renderTexture);
  }

  private void SwitchTexture()
  {
    Graphics.Blit(renderTexture, tmpTexture, mat);
    Graphics.Blit(tmpTexture, renderTexture);
  }

  public void PauseGame(bool toPause)
  {
    gamePaused = toPause;
  }

  private void DisplayFPS()
  {
    textFPS.text = nbFPS.ToString() + " FPS";
    timerFPS = Time.time;
    nbFPS = 0;
  }

  public void SetSize(int newSize)
  {
    size = newSize;
    mat.SetInt("_Cells", size);
  }
}
