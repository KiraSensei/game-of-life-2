﻿Shader "Custom/GameOfLife"{
  Properties{
      _MainTex("Albedo (RGB)", 2D) = "white" {}
      _Cells("Number of Cells", Range(1, 1000)) = 100
  }
  Subshader{
      Pass{
          CGPROGRAM
          #pragma vertex vert_img
          #pragma fragment frag

          #include "UnityCG.cginc"

          uniform sampler2D _MainTex;
          uniform float _Cells;
          
          static int2 rule[8] =
          {
            int2(0,0),
            int2(0,0),
            int2(1,0),
            int2(0,1),
            int2(0,0),
            int2(0,0),
            int2(0,0),
            int2(0,0)
          };

          fixed4 frag(v2f_img i) : SV_Target{
              float2 uv = i.uv;
              float width = 1 / _Cells;

              float pixel1 = tex2D(_MainTex, uv + float2(-width, -width)).r;
              float pixel2 = tex2D(_MainTex, uv + float2(0, -width)).r;
              float pixel3 = tex2D(_MainTex, uv + float2(+width, -width)).r;
              float pixel4 = tex2D(_MainTex, uv + float2(-width, 0)).r;
              float pixel5 = tex2D(_MainTex, uv).r;
              float pixel6 = tex2D(_MainTex, uv + float2(+width, 0)).r;
              float pixel7 = tex2D(_MainTex, uv + float2(-width, +width)).r;
              float pixel8 = tex2D(_MainTex, uv + float2(0, +width)).r;
              float pixel9 = tex2D(_MainTex, uv + float2(+width, +width)).r;

              float neighbors = pixel1 + pixel2 + pixel3 + pixel4 + pixel6 + pixel7 + pixel8 + pixel9;
              /*float value = pixel5 * rule[neighbors][0] + rule[neighbors][1] > 0.5 ? 1 : 0;
              */
              float value = 0;
              if (pixel5 > 0.6 && neighbors > 1.5 && neighbors < 3.5) value = 1;
              else if (pixel5 < 0.6 && neighbors > 2.5 && neighbors < 3.5) value = 1;

              return float4(value, value, value, 1);
          }
          ENDCG
      }
  }
}